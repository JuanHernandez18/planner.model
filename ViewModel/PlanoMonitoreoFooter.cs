﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Planner.Model
{
    public class PlanoMonitoreoFooter
    {
        public int Camas { get; set; }
        public int Cuadros { get; set; }
        public string BlancoBiologico { get; set; }
        public decimal Incidencia { get; set; }
        public decimal Severidad { get; set; }
    }
}
